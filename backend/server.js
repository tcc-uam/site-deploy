const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const cookieParser = require("cookie-parser");

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(cookieParser());
app.use(cors({origin: ["http://localhost:3000"], credentials: true}));

// MongoDB connection
const uri = process.env.ATLAS_URI;
mongoose.connect(process.env.MONGODB_URI || uri, {useNewUrlParser: true, useCreateIndex: true});

const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
})

// Routes
const alunosRouter = require('./routes/alunos');
app.use('/alunos', alunosRouter);

const parceirosRouter = require('./routes/parceiros');
app.use('/parceiros', parceirosRouter);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static("../build"));
}

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});