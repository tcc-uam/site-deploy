const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const parceiroSchema = new Schema ({
    nome: {type: String, required: true, trim: true},
    cnpj: {type: String, required: true, unique: true, trim: true},
    email: {type: String, required: true, unique: true, trim: true},
    hash: {type: String, required: true},
    celular: {type: String, required: true},
    empresa: {type: String, required: true}
});

const Parceiro = mongoose.model('Parceiro', parceiroSchema);

module.exports = Parceiro;