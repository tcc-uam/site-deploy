const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const alunoSchema = new Schema({
    nome: {type: String, required: true},
    cpf: {type: String, required: true, unique: true},
    email: {type: String, required: true, unique: true},
    hash: {type: String, required: true},
    universidade: {type: String, required: true},
    mensalidade: {type: Number, required: true},
    curso: {type: String, required: true},
    conclusao: {type: Date, required: true},
    descricao: {type: String, required: true}
});

const Aluno = mongoose.model('Aluno', alunoSchema);

module.exports = Aluno;