const router = require('express').Router();
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
let Alunos = require('../models/aluno.model');
let Parceiro = require('../models/parceiro.model');

router.route('/add').post(async (req, res) => {
    
    const {
        nome,
        cnpj,
        email,
        senha,
        celular,
        empresa
    } = req.body
    
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(senha, salt);

    const newParceiro = new Parceiro ({
        nome,
        cnpj,
        email,
        hash,
        celular,
        empresa
    });

    const parceiroSalvo = await newParceiro.save();

});

router.route('/login').post(async (req, res) => {
    
    try {
        
        const { email, senha } = req.body;
        
        const parceiroCriado = await Parceiro.findOne({email});
        
        if (!parceiroCriado)
        return res
            .status(401).json({ errorMessage: "Email ou senha inválidos." });

        const senhaCorreta = await bcrypt.compare(senha, parceiroCriado.hash);

        if (!senhaCorreta)
        return res
            .status(401).json({ errorMessage: "Email ou senha inválidos." });
        
        const token = jwt.sign({
            empresa: parceiroCriado.empresa,
            nome: parceiroCriado.nome           
        }, process.env.JWT_SECRET)

        res.cookie("token", token, {
            httpOnly: true,
            secure: true,
            sameSite: "none",
        }).send();

    }
    
    catch(err) {
        console.error(err);
        res.status(500).send();
    }
});

router.route("/logout").get((req, res) => {
    res.cookie("token", "", {
        httpOnly: true,
        expires: new Date(0),
        secure: true,
        sameSite: "none",
    })
    .send();
});

router.get('/user', (req, res) => {
    try {
        const token = req.cookies.token;
        if (!token) return res.json(false);
        jwt.verify(token, process.env.JWT_SECRET);
        var decoded = jwt.decode(token, {complete: true });
        res.json(decoded.payload);
    } catch (err) {
        res.json(false);
    }
});

router.get('/listarAlunos', (req, res) => {

    Alunos.find({  })
        .then((data) => {
            console.log('Data: ', data);
            res.json(data);
        })
        .catch((error) => {
            console.log('error: ', daerrorta);
        });
});

module.exports = router;