const router = require('express').Router();
const bcrypt = require('bcryptjs');
const { nodeName, data } = require('jquery');
const jwt = require("jsonwebtoken");
const { findOne } = require('../models/aluno.model');
let Aluno = require('../models/aluno.model');

router.route('/').get((req, res) => {
    Alunos.find()
        .then(alunos => res.json(alunos))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post(async (req, res) => {
    
    const {
        nome,
        cpf,
        email,
        senha,
        universidade,
        mensalidade,
        curso,
        conclusao,
        descricao
    } = req.body
    
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(senha, salt);

    const newAluno = new Aluno ({
        nome,
        cpf,
        email,
        hash,
        universidade,
        mensalidade,
        curso,
        conclusao,
        descricao
    });

    const alunoSalvo = await newAluno.save();

    const token = jwt.sign({
        user: alunoSalvo._id,
    }, process.env.JWT_SECRET);

    res.cookie("token", token, {
        httpOnly: true,
        secure: true,
        sameSite: "none"
    }).send();

});

router.route('/login').post(async (req, res) => {
    
    try {
        
        const { email, senha } = req.body;
        
        const alunoCriado = await Aluno.findOne({email});
        
        if (!alunoCriado)
        return res
            .status(401).json({ errorMessage: "Email ou senha inválidos." });

        const senhaCorreta = await bcrypt.compare(senha, alunoCriado.hash);

        if (!senhaCorreta)
        return res
            .status(401).json({ errorMessage: "Email ou senha inválidos." });
        
        const token = jwt.sign({
            cpf: alunoCriado.cpf,
            nome: alunoCriado.nome,
            email: alunoCriado.email,
            curso: alunoCriado.curso,
            descricao: alunoCriado.descricao,
            universidade: alunoCriado.universidade
        }, process.env.JWT_SECRET)

        res.cookie("token", token, {
            httpOnly: true,
            secure: true,
            sameSite: "none",
        }).send();

    }
    
    catch(err) {
        console.error(err);
        res.status(500).send();
    }
});

router.route("/logout").get((req, res) => {
    res.cookie("token", "", {
        httpOnly: true,
        expires: new Date(0),
        secure: true,
        sameSite: "none",
    })
    .send();
});

router.get("/loggedIn", (req, res) => {
    try {
        const token = req.cookies.token;
        if (!token) return res.json(false);
        jwt.verify(token, process.env.JWT_SECRET);
        res.send(true);
    } catch (err) {
        res.json(false);
    } 
});

router.get('/user', (req, res) => {
    try {
        const token = req.cookies.token;
        if (!token) return res.json(false);
        jwt.verify(token, process.env.JWT_SECRET);
        var decoded = jwt.decode(token, {complete: true });
        res.json(decoded.payload);
    } catch (err) {
        res.json(false);
    }
});

module.exports = router;