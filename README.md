# Como clonar repositório

Copie o comando de clone: `git clone https://tcc-uam-admin@bitbucket.org/tcc-uam/site-template.git`

1. Instale o [GIT](https://git-scm.com/downloads).
2. Uma vez instalado, escolha o diretório no qual deseja clonar o repositório e clique com o botão direito do mouse.
3. Selecione a opção "Git Bash Here".
4. Aperte "Shift + Ins" para colar o comando de clone.
5. Aperte ENTER.

# Como executar o projeto

Após clonar o repositório, siga o passo a passo a seguir:

- ## Instalando NodeJS

	Verifique se o NodeJS está instalado na sua máquina usando o comando `node -v` no terminal.

	Caso não tenha, instale a útlima versão [aqui](https://nodejs.org/en/download/).

- ## Instalando Dependências

	Utilize o comando `npm install` do NodeJS no terminal para realizar a instalação dos pacotes abaixo.

	`npm install react react-dom react-router-dom react-scripts`

	`npm install axios bcryptjs cookie-parser cookieparser jsonwebtoken`

	`npm install react-bootstrap bootstrap bootstrap bootstrap-fileinput qrcode.react jquery`

- ## Conectando com o banco de dados

	Execute o **Windows PowerShell** como administrador e rode o comando `Set-ExecutionPolicy RemoteSigned -Scope CurrentUser`

	Abra o terminal no diretório *backend* e execute:

	1. `npm install nodemon -g`
	2. `npm install cors dotenv express mongoose`

- ## Executando

	1. Abra o terminal no diretório *backend* e execute: `nodemon server`


	2. Abra outro terminal no diretório *tcc-mern* e execute: `npm start`