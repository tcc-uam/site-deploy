import React from 'react';
import axios from 'axios';
import './App.css';

import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./components/home.component";
import Sobre from "./components/sobre.component";
import Navbar from "./components/navbar.component";
import Alunos from "./components/alunos.component";
import Sucesso from "./components/sucesso.component";
import Cadastro from "./components/cadastro.component";
import Parceiros from "./components/parceiros.component";
import loginAluno from "./components/loginAluno.component";
import Solicitacao from './components/solicitacao.component';
import perfilAluno from "./components/perfilAluno.component";
import loginParceiro from "./components/loginParceiro.component";
import perfilParceiro from "./components/perfilParceiro.component";
import CadastroParceiro from "./components/cadastroParceiro.component";
import { AuthContextProvider } from './context/AuthContext';

axios.defaults.withCredentials = true;

function App() {
  return (
    <AuthContextProvider>
    <Router>
      <Navbar/>
      <Route path="/" exact component={Home}/>
      <Route path="/sobre" component={Sobre}/>
      <Route path="/alunos" component={Alunos}/>
      <Route path="/sucesso" component={Sucesso}/>
      <Route path="/cadastro" component={Cadastro}/>
      <Route path="/parceiros" component={Parceiros}/>
      <Route path="/loginAluno" component={loginAluno}/>
      <Route path="/solicitacao" component={Solicitacao}/>
      <Route path="/perfilAluno" component={perfilAluno}/>
      <Route path="/loginParceiro" component={loginParceiro}/>
      <Route path="/perfilParceiro" component={perfilParceiro}/>
      <Route path="/cadastroParceiros" component={CadastroParceiro}/>
      
    </Router>
    </AuthContextProvider>
  );
}

export default App;