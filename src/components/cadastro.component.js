import React, { Component } from 'react';
import axios from 'axios';

export default class Cadastro extends Component {

    constructor(props) {
        
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeNome = this.onChangeNome.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeSenha = this.onChangeSenha.bind(this);
        this.onChangeCPF = this.onChangeCPF.bind(this);
        this.onChangeUniversidade = this.onChangeUniversidade.bind(this);
        this.onChangeCurso = this.onChangeCurso.bind(this);
        this.onChangeConclusao = this.onChangeConclusao.bind(this);
        this.onChangeMensalidade = this.onChangeMensalidade.bind(this);
        this.onChangeDescricao = this.onChangeDescricao.bind(this);

        this.state = {
            nome: '',
            email: '',
            senha: '',
            cpf: '',
            universidade: '',
            curso: '',
            conclusao: '',
            mensalidade: 0,
            descricao: ''
        }
    }

    onChangeNome(e) {
        this.setState({
            nome: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangeSenha(e) {
        this.setState({
            senha: e.target.value
        });
    }

    onChangeCPF(e) {
        this.setState({
            cpf: e.target.value
        });
    }

    onChangeUniversidade(e) {
        this.setState({
            universidade: e.target.value
        });
    }

    onChangeCurso(e) {
        this.setState({
            curso: e.target.value
        });
    }

    onChangeConclusao(e) {
        this.setState({
            conclusao: e.target.value
        });
    }

    onChangeMensalidade(e) {
        this.setState({
            mensalidade: e.target.value
        });
    }

    onChangeDescricao(e) {
        this.setState({
            descricao: e.target.value
        });
    }

    onSubmit(e) {

        e.preventDefault();

        const aluno = {
            nome: this.state.nome,
            email: this.state.email,
            senha: this.state.senha,
            cpf: this.state.cpf,
            universidade: this.state.universidade,
            curso: this.state.curso,
            conclusao: this.state.conclusao,
            mensalidade: this.state.mensalidade,
            descricao: this.state.descricao
        }

        console.log(aluno);

        axios.post('http://localhost:5000/alunos/add', aluno)
            .then(res => console.log(res.data));

        window.location = '/sucesso';

    }

    render() {
        return (
            <section class="ftco-section">
            <div class="alert alert-info container" role="alert">
              <b>Importante: </b>
              Os arquivos solicitados bem como sua descrição pessoal são vitais para o nosso processo. 
              Essas informações irão aumentar suas chances de receber patrocínio e aumentar o interesse de nossos parceiros no seu perfil!
              Informe esses campos da melhor maneira possível.
            </div>
            <form onSubmit={this.onSubmit}>
                <div class="container row mx-auto mt-3" style={{backgroundColor: 'white', padding: '2em', boxShadow: '0px 10px 34px -15px rgba(0, 0, 0, 0.24)'}}>
                    <div class="col-md-6 order-md-1">
                        <h4 class="mb-3">Contato</h4>

                        <div class="row">
                            <div class="col-md-7 mb-3">
                                <label for="nome">Nome completo</label>
                                <input type="text" class="form-control" id="nome" required
                                    value={this.state.nome} onChange={this.onChangeNome} />
                                <div class="invalid-feedback">
                                    Informe seu nome.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf" required
                                    value={this.state.cpf} onChange={this.onChangeCPF} />
                                <div class="invalid-feedback">
                                    Informe seu CPF.
                    </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email"
                                    value={this.state.email} onChange={this.onChangeEmail}
                                    placeholder='Por onde faremos contato e será solicitado durante o login' />
                                <div class="invalid-feedback">
                                    Informe um e-mail válido.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                            <div class="col-md-6 mb-3">
                                    <label for="pw">Senha</label>
                                    <input type="password" class="form-control" id="pw" required
                                        value={this.state.senha} onChange={this.onChangeSenha} />
                                    <div class="invalid-feedback">
                                        Informe uma senha.
                                    </div>
                            </div>
                            
                            <div class="col-md-6 mb-3">
                                    <label for="pw">Confirmar Senha</label>
                                    <input type="password" class="form-control" id="pw"/>
                            </div>
                        </div>

                        <div class="custom-file col-md-12 mt-3">
                            <input type="file" class="custom-file-input" id='customFile'></input>
                            <label for="customFile" class='custom-file-label' style={{border: '1px solid transparent', backgroundColor: 'rgba(0, 0, 0, 0.05)'}}>
                                Histórico escolar, currículo ou carta de recomendação.
                            </label>
                        </div>

                    </div>

                    <div class="col-md-6 offset-md-0 order-2">
                        <h4 class="mb-3">Universidade</h4>

                        <div class="row">
                            <div class="col-md-8 mb-3">
                                <label for="universidade">Instituição</label>
                                <select class="form-control" id="universidade" required
                                    value={this.state.universidade} onChange={this.onChangeUniversidade}>
                                    <option>Nenhuma</option>
                                    <option>Universidade Anhembi Morumbi (UAM)</option>
                                </select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="mensalidade">Mensalidade</label>
                                <input type="number" class="form-control" id="mensalidade" required min='0' max='5000'
                                    value={this.state.mensalidade} onChange={this.onChangeMensalidade} />
                                <div class="invalid-feedback">
                                    Informe sua mensalidade.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="curso">Curso</label>
                                <select class="form-control" id="curso" required
                                    value={this.state.curso} onChange={this.onChangeCurso}>
                                    <option>Nenhum</option>
                                    <option>Sistemas de Informação</option>
                                    <option>Ciência da Computação</option>
                                    <option>Engenharia de Computação</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="semestre">Data de Conclusão</label>
                                <input class="form-control" type="month" value={this.state.conclusao} onChange={this.onChangeConclusao}/>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-12 mb-3">
                                <label for="descricao">Nos conte mais sobre você!</label>
                                <textarea class="form-control" id="descricao"
                                    placeholder="Este espaço é seu. Aproveite para escrever sobre você, seus hobbies, projetos, realizações universitárias e o porquê você precisa deste apoio :)"
                                    rows="4" required
                                    value={this.state.descricao} onChange={this.onChangeDescricao}></textarea>
                            </div>
                        </div>
                    </div>
                </div>


                

                <div class="container mt-4 mb-3">
                    <a href="/alunos" class="btn btn-link btn-lg mx-2" style={{color: "#fe5244"}}>Cancelar</a>
                    <button class="btn btn-lg mx-2" type="submit" style={{backgroundColor: "#08beb8", outlineColor: "#08beb8", color: "white"}}>Enviar</button>
                </div>
            </form>
            </section>
        )
    }
}