import React , { Component } from 'react';

export default class Apoie extends Component {
    render() {
        return (
            <div class="container mx-auto my-3">
                <div class="mx-auto p-md-5">
                    <div class="row g-xl-0 mt-5 ml-2 align-items-center">
                        <div class="col-md-6 text-md-end order-md-1">
                            <img class="img-fluid mx-auto mb-md-0" src="../../../scene2.svg" alt=""/>
                        </div>
                        <div class="col-md-6 text-md-start order-md-0">
                            <h2 class="fw-bold lh-base"  style={{fontFamily: 'Roboto'}}>
                                Faça acontecer
                            </h2>
                            <p class="pt-3">Se a sua organização possui responsabilidade social,
                            seja um parceiro! Invista em futuros profissionais qualificados para o mercado.</p>
                            <div class="py-3">
                                <a href="/loginParceiro" class="btn btn-lg rounded-pill" style={{backgroundColor: "#3fb1b5", outlineColor: "#3fb1b5", color: "white"}}>
                                    Entrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}