import React , { Component } from 'react';
import { Button, Card } from 'react-bootstrap';

export default class Solicitacao extends Component {

    render() {

        return (
            
            <div class='container'>
                <div class='row justify-content-center mt-5'>
                <Card
                    style={{ width: '25rem' }}
                    className="mb-2"
                >
                    <Card.Header style={{backgroundColor: '#28a745', color: 'white'}}>
                        Apoio solicitado!
                    </Card.Header>
                    
                    <Card.Body style={{textAlign: 'center'}}>
                        
                        <Card.Text>
                            Agora é só esperar um de nossos parceiros contribuir com o seu perfil!
                        </Card.Text>
                        <Button variant="outline-success" href="/perfilAluno">
                            OK
                        </Button>
                    </Card.Body>

                </Card>
                </div>
            </div>

        )

    }


}