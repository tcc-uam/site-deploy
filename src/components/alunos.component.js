import React , { Component } from 'react';

export default class Alunos extends Component {
    render() {
        return (
            <div class="container mx-auto my-3">
                <div class="mx-auto p-md-5">
                    <div class="row g-xl-0 mt-5 ml-2 align-items-center">
                        <div class="col-md-6 text-md-end order-md-1">
                            <img class="img-fluid mx-auto mb-md-0" src="../../../scene1.svg" alt=""/>
                        </div>
                        <div class="col-md-6 text-md-start order-md-0">
                            <h2 class="fw-bold lh-base" style={{fontFamily: 'Roboto'}}>
                                Receba apoio financeiro
                            </h2>
                            <p class="pt-3">Não abra mão da sua graduação! 
                            Faça seu cadastro e encontraremos instituições privadas que financiem 
                            seus estudos, sem endividamento.</p>
                            <div class="py-3">
                                <a href="/loginAluno" class="btn btn-lg rounded-pill" style={{backgroundColor: "#3fb1b5", outlineColor: "#3fb1b5", color: "white"}}>
                                    Entrar
                                </a>
                                <a href="/cadastro" class="btn btn-lg btn-link rounded-pill" style={{color: "#3fb1b5"}}>
                                    Cadastre-se
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}