import React, { Component } from 'react';
import axios from 'axios';

export default class Cadastro extends Component {

    constructor(props) {
        
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeNome = this.onChangeNome.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeSenha = this.onChangeSenha.bind(this);
        this.onChangeCNPJ = this.onChangeCNPJ.bind(this);
        this.onChangeEmpresa = this.onChangeEmpresa.bind(this);
        this.onChangeCelular = this.onChangeCelular.bind(this);

        this.state = {
            nome: '',
            email: '',
            senha: '',
            cnpj: '',
            empresa: '',
            celular: ''
        }

    }

    onChangeNome(e) {
        this.setState({
            nome: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangeSenha(e) {
        this.setState({
            senha: e.target.value
        });
    }

    onChangeCNPJ(e) {
        this.setState({
            cnpj: e.target.value
        });
    }

    onChangeCelular(e) {
        this.setState({
            celular: e.target.value
        });
    }

    onChangeEmpresa(e) {
        this.setState({
            empresa: e.target.value
        });
    }

    onSubmit(e) {

        e.preventDefault();

        const parceiro = {
            nome: this.state.nome,
            email: this.state.email,
            senha: this.state.senha,
            cnpj: this.state.cnpj,
            celular: this.state.celular,
            empresa: this.state.empresa
        }

        console.log(parceiro);
        
        axios.post('http://localhost:5000/parceiros/add', parceiro)
            .then(res => console.log(res.data));

        window.location = '/loginParceiro';

    }

    render() {
        return (
            <form class="container col-md-5 mt-5 pt-4" onSubmit={this.onSubmit}>
                
                <div class='row'>
                    <div class='col-md-7 py-2'>
                        <label for="empresa">Razão Social</label>
                        <input type="text" class="form-control" id="empresa" required
                            value={this.state.empresa} onChange={this.onChangeEmpresa} />
                    </div>
                    <div class='col-md-5 py-2'>
                        <label for="cnpj">CNPJ</label>
                        <input type="text" class="form-control" id="cnpj" required
                            value={this.state.cnpj} onChange={this.onChangeCNPJ} />
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-7 py-2'>
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" required
                            value={this.state.email} onChange={this.onChangeEmail} />
                    </div>
                    <div class='col-md-5 py-2'>
                        <label for="senha">Senha</label>
                        <input type="password" class="form-control" id="senha" required
                            value={this.state.senha} onChange={this.onChangeSenha} />
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-7 py-2'>
                        <label for="rep">Representante</label>
                        <input type="text" class="form-control" id="rep" required
                            value={this.state.nome} onChange={this.onChangeNome} />
                    </div>
                    <div class='col-md-5 py-2'>
                        <label for="cel">Celular</label>
                        <input type="text" class="form-control" id="cel" required
                            value={this.state.celular} onChange={this.onChangeCelular} />
                    </div>
                </div>

                <a href='/parceiros' class="btn btn-link my-4 mr-2" type="submit">Cancelar</a>
                <button class="btn btn-primary my-4" type="submit">Enviar</button>

            </form>
        )
    }
}