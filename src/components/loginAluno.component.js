import "../login.css";
import axios from "axios";
import { useHistory } from 'react-router-dom';
import AuthContext from "../context/AuthContext.js";
import React , { useContext, useState } from 'react';

function Login() { 

    const [email, setEmail] = useState("");
    const [senha, setSenha] = useState("");

    const { getLoggedIn } = useContext(AuthContext);
    const history = useHistory();

    async function login(e) {

        e.preventDefault();

        try {

            const loginData = {
                email,
                senha,
            };

            await axios.post("http://localhost:5000/alunos/login", loginData);

            await getLoggedIn();
            history.push("/perfilAluno");

        }

        catch (err) {
            console.error(err);
        }

    }

    return (
        
        <section class="ftco-section">
        <div class='container'>
            <div class='row justify-content-center'>
                <div class='col-md-12 col-lg-10'>
                    <div class='wrap d-md-flex'>
                        <div class='text-wrap p-4 p-lg-5 text-center d-flex align-items-center order-md-last'>
                            <div class='text w-100'>
                                <h2>Plano B</h2>
                                <p>&copy; 2021</p>
                            </div>
                        </div>
                        <div class='login-wrap p-4 p-lg-5'>
                            <div class='d-flex'>
                                <div class='w-100'>
                                    <h3 class='mb-4'>Olá Estudante</h3>
                                </div>
                            </div>
                            <form class='signin-form' onSubmit={login}>
                                <div class='form-group mb-3'>
                                    <label class="label" for="name">Email</label>
                                    <input type="text" class="form-control" required autofocus
                                    onChange={(e) => setEmail(e.target.value)} value={email}/>
                                </div>
                                <div class='form-group mb-3'>
                                    <label class="label" for="name">Senha</label>
                                    <input type="password" class="form-control" required
                                    onChange={(e) => setSenha(e.target.value)} value={senha}/>
                                </div>
                                <div class='form-group'>
                                    <button type="submit" class="form-control btn mt-3" style={{backgroundColor: '#08beb8', color: 'white'}}>Sign In</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>                      
        </section>

  );
}
export default Login;