import axios from 'axios';
import QRCode from 'qrcode.react';
import React , { Component } from 'react';
import { Button, Modal, Card } from 'react-bootstrap';

export default class Home extends Component {

    state = {
        representante: undefined,
        empresa: undefined,
        show: false,
        alunos: []
    };

    handleModal() {
        
        this.setState({ show: !this.state.show })

        setTimeout(() => {
            this.setState({ show: false });
        }, 3000);

        setTimeout(() => {
            alert('Aluno apoiado!');
        }, 4000);

    }

    mesesACursar(dataConclusao) {
        var meses;
        var hoje = new Date();
        var conclusao = new Date(dataConclusao);
        meses = (conclusao.getFullYear() - hoje.getFullYear()) * 12;
        meses -= hoje.getMonth();
        meses += conclusao.getMonth();
        return meses <= 0 ? 0 : meses + 1;
    }

    async componentDidMount() {
        const response = await axios.get("http://localhost:5000/parceiros/user");
        this.setState({ empresa: response.data.empresa, representante: response.data.nome });
     
    }    
    
    getAlunos = () => {
    axios.get('http://localhost:5000/parceiros/listarAlunos')
        .then((response) => {
            const data = response.data;
            this.setState({ alunos: data });
            console.log('Dados recebidos');
        })
        .catch(() => {
            alert('Deu ruim parceiro...');
        });
    }

    displayAlunos = (alunos) => {

        if (!alunos.length) return <p class='text-center mt-5'>Nada por aqui... <br/> Pesquise estudantes por curso!</p>;
    
        return alunos.map((aluno, index) => (
            <div key={index} className="card-aluno__display" class='pb-3'>
                <Card>
                    <Card.Body>
                        <Card.Title>
                            <h4>{aluno.nome.split(' ', 1)} precisa de R${this.mesesACursar(aluno.conclusao) * aluno.mensalidade}</h4>
                        </Card.Title>
                        <Card.Subtitle>
                            <p class='text-secondary'>Cursando {aluno.curso}</p>
                        </Card.Subtitle>
                        <Card.Text>
                            <p>{aluno.descricao}</p>
                            <button class="btn mr-2" style={{backgroundColor:'dodgerblue', color:'white'}}><i class="fa fa-download"/> historico.pdf</button>
                        </Card.Text>
                        <Button onClick={() => {this.handleModal()}} style={{border: 'none', backgroundColor: "#08beb8", color: "white", float:'right'}} block>Apoiar</Button>
                    </Card.Body>
                </Card>
            </div>
        ));
    };

    render() {
        return (
            
            <div>
                
                <div class="container mt-3 pt-3">
                    
                    <div class='row'>
                        
                        <div class='col-md-4'>
                            <div class='container text-center pb-3'>
                                <Card >
                                    <Card.Img variant="top" src="../../../perfilParceiroNU.png" />
                                    <Card.Body>
                                        <Card.Title>
                                            <h2 class="mt-3 py-2"> {this.state.empresa} </h2>
                                        </Card.Title>
                                        <Card.Subtitle>
                                            <h5 class='text-secondary'> {this.state.representante} </h5>
                                        </Card.Subtitle>
                                    </Card.Body>
                                </Card>
                            </div>
                        </div>
                        
                        <div class='col-md-8'>
                            
                            <div class='container'>
                                
                                <div class='row mb-3'>
                                    <div class='col-md-10'>
                                        <select class="form-control">
                                            <option>Selecione um curso</option>
                                            <option>Sistemas de Informação</option>
                                        </select>
                                    </div>
                                    <div class='col-md-2'>
                                        <Button onClick={() => {this.getAlunos()}}>Pesquisar</Button>
                                    </div>
                                </div>


                                <div className="card-">
                                    {this.displayAlunos(this.state.alunos)}
                                </div>

                                <Modal show={this.state.show}>
                                    <Modal.Header>Apoie esse aluno</Modal.Header>
                                    <Modal.Body>
                                        <div class='text-center'>
                                            <QRCode value="Fundos Plano B" size='200' includeMargin={true}/>
                                        </div>
                                        <p class='text-center'>O valor ficará sob nossa custódia e será repassado diretamente à universidade.</p>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button onClick={() => {this.handleModal()}}>Fechar</Button>
                                    </Modal.Footer>
                                </Modal>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        )
    }
}