import React , { Component } from 'react';
import { Button, Card } from 'react-bootstrap';

export default class Sucesso extends Component {

    render() {

        return (
            
            <div class='container'>
                <div class='row justify-content-center mt-5'>
                <Card
                    style={{ width: '18rem' }}
                    className="mb-2"
                >
                    <Card.Header style={{backgroundColor: '#28a745', color: 'white'}}>
                        Sucesso!
                    </Card.Header>
                    
                    <Card.Body style={{textAlign: 'center'}}>
                        <Card.Title>
                            Cadastro realizado
                        </Card.Title>
                        <Button variant="outline-success" href="/loginAluno">
                            Fazer Login
                        </Button>
                    </Card.Body>

                </Card>
                </div>
            </div>

        )

    }


}