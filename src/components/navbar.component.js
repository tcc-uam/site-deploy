import axios from 'axios';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import AuthContext from '../context/AuthContext';

export default class Navbar extends Component {
    
    static contextType = AuthContext
    
    logOut() {

        axios.get("http://localhost:5000/alunos/logout");
        window.location = '/';

    }

    render() {
        const { loggedIn } = this.context;
        return (
            
            <nav class="navbar navbar-expand-lg navbar-light bg-light shadow" style={{zIndex:'100'}}>
                
                <Link to="/" class="navbar-brand">
                    <img src="../../../logotipo.png" alt=""/>
                </Link>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navPrincipal" aria-controls="navPrincipal" aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navPrincipal">
                    <ul class="navbar-nav mr-auto">
                    { loggedIn === false && (<>
                        <li class="nav-item">
                            <Link to="/alunos" class="nav-link">Estudantes</Link>
                        </li>
                        <li class="nav-item">
                            <Link to="/parceiros" class="nav-link">Parceiros</Link>
                        </li>    
                    </>)}
                    </ul>
                    { loggedIn === false && (<>
                    <a href='/sobre' type="button" class="btn mr-0" style={{backgroundColor: '#89d0ca'}}>Entrar</a> 
                    </>)}
                    { loggedIn === true && (<>
                        <button type="button" class="btn btn-outline-danger mr-0" onClick={this.logOut}>Log out</button>
                    </>)}    
                </div>
                
            </nav>
        )
    }
}