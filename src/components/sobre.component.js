import React, { Component } from 'react';
import { Card, Button, CardDeck } from 'react-bootstrap';

export default class Sobre extends Component {
    render() {

        const cardStyle = {
            background: 'none',
            border: 'none'
        }

        return (
            
            <div class='container'>
                <div class='row align-items-center mx-auto' style={{height: '90vh'}}>
                    
                    <CardDeck>
                        
                        <Card style={cardStyle}>
                            <Card.Body>
                                <Card.Title>
                                    <h4>Sou Estudante</h4>
                                </Card.Title>
                                <Card.Img src="../../../scene5.svg"/>
                                <Card.Text>
                                    <p>Se você está cursando ensino superior e necessita de apoio 
                                    financeiro para completar sua graduação, cadastre-se e encontre empresas
                                    interessadas no seu perfil e dispostas a pagar pelo seu ensino!</p>
                                </Card.Text>
                                <Button href='/loginAluno' variant='btn rounded-pill' style={{backgroundColor: '#3fb1b5', color: 'white'}}>Comece por aqui</Button>
                            </Card.Body>
                        </Card>
                    
                        <Card style={cardStyle}>
                            <Card.Body>
                                <Card.Title>
                                    <h4>Sou Parceiro</h4>
                                </Card.Title>
                                <Card.Img src="../../../scene9.svg"/>
                                <Card.Text>
                                    <p>A Responsabilidade Social gera um grande impacto positivo para a comunidade na qual sua
                                    organização está inserida, patrocine futuros promissores e ajude a aquecer o mercado
                                    de trabalho!</p>
                                </Card.Text>
                                <Button href='/loginParceiro' variant='btn rounded-pill' style={{backgroundColor: '#3fb1b5', color: 'white'}}>Comece por aqui</Button>
                            </Card.Body>
                        </Card>

                    </CardDeck>

                </div>
            </div>

        )
    }
}