import axios from 'axios';
import React , { Component } from 'react';
import { ListGroup, Tabs, Tab } from 'react-bootstrap';

export default class Home extends Component {

    state = {
        nome: undefined,
        curso: undefined,
        universidade: undefined
    };

    async componentDidMount() {
        const response = await axios.get("http://localhost:5000/alunos/user");
        this.setState({ nome: response.data.nome });
        this.setState({ curso: response.data.curso });
        this.setState({ email: response.data.email });
        this.setState({ cpf: response.data.cpf });
        this.setState({ descricao: response.data.descricao });
        this.setState({ universidade: response.data.universidade });
    }

    render() {
        return (
            <div class='container mt-5'>

                <div class='row shadow justify-content-md-center mx-auto' style={{height: '80vh', borderRadius: '10px', backgroundColor: 'white'}}>
                    
                    <div class='col-md-4 text-center' style={{height: '100%'}}>
                        <img class="rounded img-fluid mt-5 pt-5" src="../../../perfil.png" height="200px" width="200px" alt=""/>
                        <h2 class="mt-2 py-3"> {this.state.nome} </h2>
                        <ListGroup>
                            <ListGroup.Item action href="/solicitacao">Solicitar Apoio</ListGroup.Item>
                        </ListGroup>
                    </div>

                    <div class='col-md-8' style={{ borderLeft: '1px solid lightgrey' }}>
                        <div class='ml-5 my-5 py-5'>
                        <Tabs defaultActiveKey="perfil">
                            <Tab eventKey="perfil" title="Perfil">
                                <h4 class='mt-5'>Instituição</h4>
                                <p>{this.state.universidade}</p>
                                <h4>Curso</h4>
                                <p>{this.state.curso}</p>
                                <h4>Descricao</h4>
                                <p>{this.state.descricao}</p>
                            </Tab>
                            <Tab eventKey="dados" title="Dados">
                                <p class='mt-5'><b>E-mail: </b>{this.state.email}</p>
                                <p><b>CPF: </b>{this.state.cpf}</p>
                            </Tab>
                            <Tab eventKey="apoiadores" title="Meus Apoiadores">
                                <p class='mt-5'>Nubank apoiou você!</p> 
                            </Tab>
                        </Tabs>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
}