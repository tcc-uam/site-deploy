import React, { Component } from 'react';
import "../home.css";

export default class Home extends Component {
    render() {
        return (
            
            <div class="container-fluid">
            
                <div class='row'>
            
                    <div class="col-md-12">
                        
                        <div class="row " style={{height: '100vh'}}>
                            <div class="col-md-6 text-center" id="texto">
                                <div class='container'>
                                    <h2 id='title'>Somos seu Plano B</h2>
                                    <p class="lead">
                                        Conectamos grandes empresas com estudantes brilhantes!
                                        Cadastre-se e encontre organizações que acreditem
                                        no seu potencial e patrocinem sua jornada.
                                    </p>
                                    <a href="#section2" class="btn btn-lg rounded-pill" id='saibaMais'>Saiba mais</a>
                                </div>
                            </div>
                            <div class="col-md-6" id="garota"/>
                        </div>
                        
                        <div class="row align-items-center" style={{height: '70vh', backgroundColor: '#f6f6f6'}}>
                            <div class="col-md-6">
                                <div id='section2'/>
                            </div>
                            <div class="col-md-6">
                                <div class='container'>
                                    <h2 style={{fontFamily: 'Roboto'}}>Parceiros</h2>
                                    <p style={{fontFamily: 'Lato'}}>A Responsabilidade Social gera um grande impacto positivo para a comunidade na qual sua organização está inserida, 
                                    patrocine futuros promissores e ajude a aquecer o mercado de trabalho!</p>
                                    <a href="/parceiros" class="btn btn-lg rounded-pill" style={{backgroundColor: "#3fb1b5", outlineColor: "#3fb1b5", color: "white"}}>
                                        Faça Parte
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="row align-items-center"  style={{height: '70vh', backgroundColor: '#89d0ca'}}>
                            <div class="col-md-6">
                                <div class='container ml-5'>
                                    <h2 style={{fontFamily: 'Roboto', color: 'white'}}>Estudantes</h2>
                                    <p style={{fontFamily: 'Lato'}}>Se você está cursando ensino superior e necessita de apoio financeiro para completar sua graduação, 
                                    cadastre-se e encontre empresas interessadas no seu perfil e dispostas a pagar pelo seu ensino!</p>
                                    <a href="/alunos" class="btn btn-lg rounded-pill" style={{backgroundColor: "white", outlineColor: "#3fb1b5", color: "black"}}>
                                        Faça Parte
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='' id='section3'/>
                            </div>
                        </div>

                        <footer class="bg-light text-center text-lg-start">
                            <div class="text-center p-3">
                                Plano B © 2021
                            </div>
                        </footer>

                    </div>

                </div>

            </div>
            
        )
    }
}